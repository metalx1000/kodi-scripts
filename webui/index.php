<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <!--
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <?php include("fav.php");?>
    <style>
      #main, p{
        margin-top: 20px;
      }

      .slidecontainer {
        width: 100%;
      }

      .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 25px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
      }

      .slider:hover {
        opacity: 1;
      }

      .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
      }

      .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        background: #4CAF50;
        cursor: pointer;
      }
    </style>
    <script>
      //Good Reference
      //http://habitech.s3.amazonaws.com/PDFs/STR/STREAM_Box%20Note%20-%20JSON%20API%20Examples%20for%20ContentPlayer%20(Isengard).pdf

      var ip="192.168.1.146";
      var irip="192.168.1.12";
      var port="8080";


      $(document).ready(function(){

        $(".navi").click(nav);

        $(".tv").click(TV);

        //setInterval(status,1000);

        //volume slider
        var slider = document.getElementById("myRange");
        slider.oninput = function() {
          volume(this.value);
        }
      });

      function TV(){
        var d = $(this).attr('cmd');
        var url="http://"+irip+"/ir?code="+d;
        $.get(url);
      }

      function volume(v){
        var cmd="volume"
        $.get('http://' + ip + ":" + port + "/cgi-bin/" + cmd + ".cgi?vol=" + v, function(data){
          console.log(data); 
        })
      }


      function nav(){
        var cmd = $(this).attr('cmd');
        //var cmd = "play_pause.cgi";
        $.get('http://' + ip + ":" + port + "/cgi-bin/" + cmd + ".cgi", function(data){
          console.log(data); 
        })
      }

      function seek(){
        var d = $(this).attr('cmd');
        for(var i = 0;i<2;i++){
          $.ajax({
            type: 'POST',
            url: 'http://' + ip + ":" + port + '/jsonrpc',
            dataType: 'jsonp',
            jsonpCallback: 'jsonCallback',
            type: 'POST',
            async: true,
            timeout: 5000,
            data: 'request=' + encodeURIComponent( '{"jsonrpc": "2.0", "method": "Player.Seek", "params": {"value": "'+d+'", "playerid": '+i+'}, "id": 1}' )
            //    data: 'request=' + encodeURIComponent( '{"jsonrpc": "2.0", "method": "Player.Seek", "params": {"value": "'+d+'"}, "id": 1}' )
          });
        }

      }

      function status(){
        for(var i = 0;i<2;i++){
          $.ajax({
            type: 'POST',
            url: 'http://' + ip + ":" + port + '/jsonrpc',
            dataType: 'jsonp',
            //      jsonpCallback: 'jsonCallback',
            type: 'POST',
            async: true,
            timeout: 5000,
            data: 'request=' + encodeURIComponent( '{"jsonrpc":"2.0","id":2,"method":"Player.GetProperties","params":{ "playerid":'+i+',"properties":["speed", "shuffled", "repeat", "subtitleenabled", "time", "totaltime", "position", "currentaudiostream", "partymode" ] } }' )
            }).done(function(data){
            if(data.result !== undefined){
              var s=data.result;
              var hour = leadZero( s.time.hours );
              var min = leadZero( s.time.minutes );
              var sec = leadZero( s.time.seconds );

              //total time
              var thour = leadZero( s.totaltime.hours );
              var tmin = leadZero( s.totaltime.minutes );
              var tsec = leadZero( s.totaltime.seconds );

              var stat=hour+":"+min+":"+sec+" of "+thour+":"+tmin+":"+tsec;
              $("#status").html(stat);
              document.title = stat;
            }

          });

        }
      }

      function leadZero(n){
        return n > 9 ? "" + n: "0" + n;
      }

    </script>
  </head>
  <body>

    <div id="main" class="container">
      <div class="row">
        <div id="status"></div>
        <div class="btn-group btn-group-justified">
          <a href="#" class="btn btn-primary btn-danger tv" cmd="E0E040BF">POWER </a>
          <a href="#" class="btn btn-primary tv" cmd="E0E0807F">SOURCE</a>
          <a href="#" class="btn btn-primary tv" cmd="E0E058A7">MENU</a>
          <a href="#" class="btn btn-primary tv" cmd="E0E0D02F">Vol-</a>
          <a href="#" class="btn btn-primary tv" cmd="E0E0E01F">Vol+</a>
        </div>
        <br>
        <button type="button" cmd="play_pause" class="btn btn-primary btn-block navi">Play/Pause</button>
        <button type="button" cmd="stop" class="btn btn-primary btn-block navi">Stop</button>
        <p style="margin-top: 5px;">Volume:</p>
        <input type="range" min="1" max="100" value="50" class="slider" id="myRange">         

        <br>
        <div class="btn-group btn-group-justified" style="bottom: 5px;">
          <a href="#" class="btn btn-primary navi" cmd="up">UP</a>
          <a href="#" class="btn btn-primary navi" cmd="down">DOWN</a>
        </div>
        <!--
        <button type="button" class="btn btn-primary btn-block navi" cmd="up">UP</button>
        <button type="button" class="btn btn-primary btn-block navi" cmd="down">DOWN</button>
        -->
        <div>

          <div style="bottom: 5px;margin-bottom: 5px;">
            <button type="button" class="btn btn-primary btn-block navi" cmd="select">SELECT</button>
            <button type="button" class="btn btn-primary btn-block navi" cmd="back">BACK</button>
          </div>
        </div>
        <div class="btn-group btn-group-justified">
          <a href="#" class="btn btn-primary navi" cmd="smallback"> < </a>
          <a href="#" class="btn btn-primary navi" cmd="bigback"> << </a>
          <a href="#" class="btn btn-primary navi" cmd="bigforward"> >> </a>
          <a href="#" class="btn btn-primary navi" cmd="smallforward"> > </a>
        </div>
        <br>

        <button type="button" class="btn btn-primary btn-block navi" cmd="osd">ShowOSD</button>
      </div>

    </div>
  </body>
</html>

